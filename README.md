# Caverns of Wintermoor

> A simple roguelike implemented with python and libtcod

## Table of contents

- [General info](#general-info)
- [Controls](#controls)
- [Screenshots](#screenshots)
- [Technologies](#technologies)
- [Setup](#setup)
- [Features](#features)
- [Status](#status)
- [Inspiration](#inspiration)
- [Contact](#contact)

## General Info

As the name would imply, this is a roguelike made with python! Currently there isn't much of a story, but the win condition is reaching the 10th floor staircase.

## Controls

Do you like vi keys? I like vi keys, but not everybody does! Here's the list of keybindings for the game:

### Movement

- Arrow keys (cardinal directions) and home, end, page up, page down (diagonals)
- Vi keys (cardinal directions), y, u, b, n (diagonals)
- Numpad (each key around 5 is the corresponding direction)

### Wait Keys

- Period
- Numpad 5
- Clear key

#### Confim Keys

- Enter
- Numpad Enter

### Other Useful Keys

- Escape to quit
- g to pick up items
- v to open message history
- i to open the inventory
- d to select an item to drop
- c to open the character info screen
- \> to descend a staircase
- / to give a way to look around without the mouse
- ? to open a help menu with these keybindings

## Screenshots

Coming Soon!

## Technologies

- [Poetry](https://python-poetry.org/): Dependency and venv management
- [Python libtcod bindings](https://github.com/libtcod/python-tcod): Library for handling user input, window generation, and some useful roguelike niceties
- [Numpy](https://numpy.org/): For creating struct like types and efficient arrays

## Setup

### Development Setup

#### Poetry

If you have Poetry installed, you can simply clone this repository with git and then run `poetry install` from within the directory and the dependencies will be installed into a virtual environment for you. Note that if you are running Linux you have to install libsdl2 version 2.0.5+ from your package manager. On Ubuntu I did this with `sudo apt install libsdl2-dev`.

#### Requirements.txt

I don't have much experience with other dependency management setups. I would create a virtual environment with your tool of choice and then run `pip3 (or pip) install -r requirements.txt` to install the dependencies into the virtual environment.

### Installation to Play

#### pipx

If you find yourself installing lots of standalone Python programs with conflicting dependencies, give [pipx](https://pypi.org/project/pipx/) a try. It installs and runs programs in their own virtual environments, making them easy to access from the command line and ensures they don't have dependency version conflicts.

This game isn't on pypi, so it can be installed with:

`pipx install git+https://gitlab.com/Silent__Ninja346/python-roguelike.git`

#### pip

If pipx isn't your thing, you should be able to use the following to install the game:

`pip install --user git+https://gitlab.com/Silent__Ninja346/python-roguelike.git`

## Features

List of features ready and TODOs for future development

- Endless procedurally generated floors with increasing difficulty and item drops.
- An [Entity Component System](https://en.wikipedia.org/wiki/Entity_component_system)
- A stacking message system with a viewable history
- A 26 item inventory that holds found items and equipment
- Game saves made on quit that can be loaded at next startup
- Win condition on surviving 10 floors of the dungeon

To-do list:

- Modify procedural generation to better utilize map space
- Possibly new allies
- Flesh the final goal out further
- Multiple save files
- Different starting classes

## Status

As of now I am porting this game, or something similar to it, over to Rust because I enjoy using the language. This repository isn't going anywhere, but I likely won't keep updating it if the Rust version comes out good.

## Inspiration

This project is heavily influenced by games like Rogue, NetHack, and Angband. If you like roguelikes go play the ones that started it all! Most of the code from the first version of this game came from following the [The Complete Roguelike Tutorial](http://www.rogueliketutorials.com/tutorials/tcod/v2/), and it has been expanded on and changed to create what you see here.

## Contact

Created by [David Krauthamer](https://davidkrauthamer.com) - feel free to contact me!
