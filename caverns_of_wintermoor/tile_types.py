from typing import Tuple

import numpy as np

# creates a custom data type that acts like a struct from C or Rust
# this will be structured type compatible with Console.tiles_rgb
graphic_dt = np.dtype(
    [
        ("ch", np.int32),  # will turned into a unicode character
        ("fg", "3B"),  # means 3 unsigned bytes, which we can use for rgb colors
        ("bg", "3B"),
    ]
)

# this will get used for statically defined tile data
tile_dt = np.dtype(
    [
        ("walkable", np.bool_),  # true if this tile can be walked through
        ("transparent", np.bool_),  # true if this tile doesn't block line of sight
        ("dark", graphic_dt),  # graphics for when this tile is out of line of sight
        ("light", graphic_dt),  # graphics for when this tile is in line of sight
    ]
)


def new_tile(
    *,  # this will force the use of keywords, so parameter order won't mater
    walkable: int,
    transparent: int,
    dark: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]],
    light: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]],
) -> np.ndarray:
    """helper function for defining individual tile types"""
    return np.array((walkable, transparent, dark, light), dtype=tile_dt)


# unexplored and unseen tiles
SHROUD = np.array((ord(" "), (255, 255, 255), (0, 0, 0)), dtype=graphic_dt)

floor = new_tile(
    walkable=True,
    transparent=True,
    dark=(
        ord(" "),
        (255, 255, 255),
        (50, 50, 150),
    ),
    light=(
        ord(" "),
        (255, 255, 255),
        (200, 180, 50),
    ),
)

wall = new_tile(
    walkable=False,
    transparent=False,
    dark=(
        ord(" "),
        (255, 255, 255),
        (0, 0, 100),
    ),
    light=(
        ord(" "),
        (255, 255, 255),
        (130, 110, 50),
    ),
)

down_stairs = new_tile(
    walkable=True,
    transparent=True,
    dark=(
        ord(">"),
        (0, 0, 100),
        (50, 50, 100),
    ),
    light=(
        ord(">"),
        (255, 255, 255),
        (200, 180, 50),
    ),
)
