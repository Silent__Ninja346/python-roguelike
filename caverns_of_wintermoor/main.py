#!/usr/bin/env python3
import traceback
from pathlib import Path

import tcod

from caverns_of_wintermoor import color, exceptions, input_handlers, setup_game


def save_game(handler: input_handlers.BaseEventHandler, filename: Path) -> None:
    """If the current event andler has an active Engine then save it."""
    if isinstance(handler, input_handlers.EventHandler):
        handler.engine.save_as(filename)
        print("Game saved.")


def main() -> None:
    screen_width = 80
    screen_height = 50

    tileset = tcod.tileset.load_tilesheet(
        str(Path(__file__).parents[1] / "arial10x10.png"),
        32,
        8,
        tcod.tileset.CHARMAP_TCOD,
    )

    handler: input_handlers.BaseEventHandler = setup_game.MainMenu()

    with tcod.context.new_terminal(
        screen_width,
        screen_height,
        tileset=tileset,
        title="Caverns of Wintermoor",
        vsync=True,
    ) as context:
        root_console = tcod.Console(screen_width, screen_height, order="F")
        try:
            while True:
                root_console.clear()
                handler.on_render(console=root_console)
                context.present(root_console)

                try:
                    for event in tcod.event.wait():
                        context.convert_event(event)
                        handler = handler.handle_events(event)
                except Exception:  # handle exceptions in the game
                    traceback.print_exc()  # print error to stderr
                    # then print the error to the message log
                    if isinstance(handler, input_handlers.EventHandler):
                        handler.engine.message_log.add_message(
                            traceback.format_exc(), color.error
                        )

        except exceptions.QuitWithoutSaving:
            raise
        except SystemExit:  # save and quit
            save_game(
                handler,
                Path(__file__).parents[1] / "savegame.sav",
            )
            raise
        except BaseException:  # save on any other unexpected exception
            save_game(
                handler,
                Path(__file__).parents[1] / "savegame.sav",
            )
            raise


if __name__ == "__main__":
    main()
