from caverns_of_wintermoor.components import consumable, equippable
from caverns_of_wintermoor.components.ai import HostileEnemy
from caverns_of_wintermoor.components.equipment import Equipment
from caverns_of_wintermoor.components.fighter import Fighter
from caverns_of_wintermoor.components.inventory import Inventory
from caverns_of_wintermoor.components.level import Level
from caverns_of_wintermoor.entity import Actor, Item

# player character
player = Actor(
    char="@",
    color=(255, 255, 255),
    name="Player",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=30, base_defense=1, base_power=2),
    inventory=Inventory(capacity=26),
    level=Level(level_up_base=200),
)

# lower level enemies
goblin = Actor(
    char="g",
    color=(230, 0, 255),
    name="Goblin",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=6, base_defense=0, base_power=3),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=35),
)

kobold = Actor(
    char="k",
    color=(0, 200, 200),
    name="Kobold",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=8, base_defense=0, base_power=4),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=55),
)

orc = Actor(
    char="o",
    color=(63, 127, 63),
    name="Orc",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=12, base_defense=0, base_power=4),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=75),
)


# mid tier enemies
troll = Actor(
    char="T",
    color=(0, 127, 0),
    name="Troll",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=16, base_defense=1, base_power=6),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=100),
)

hobgoblin = Actor(
    char="H",
    color=(93, 138, 168),
    name="Hobgoblin",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=20, base_defense=2, base_power=4),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=115),
)

# high tier enemies

giant = Actor(
    char="G",
    color=(37, 115, 255),
    name="Giant",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=30, base_defense=4, base_power=8),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=140),
)

dragon = Actor(
    char="D",
    color=(255, 48, 0),
    name="Dragon",
    ai_cls=HostileEnemy,
    equipment=Equipment(),
    fighter=Fighter(hp=50, base_defense=6, base_power=10),
    inventory=Inventory(capacity=0),
    level=Level(xp_given=300),
)

health_potion = Item(
    char="!",
    color=(127, 0, 255),
    name="Health Potion",
    consumable=consumable.HealingConsumable(amount=4),
)

lightning_scroll = Item(
    char="~",
    color=(255, 255, 0),
    name="Lightning Scroll",
    consumable=consumable.LightningDamageConsumable(damage=20, maximum_range=5),
)

confusion_scroll = Item(
    char="~",
    color=(207, 63, 255),
    name="Confusion Scroll",
    consumable=consumable.ConfusionConsumable(number_of_turns=10),
)

fireball_scroll = Item(
    char="~",
    color=(255, 0, 0),
    name="Fireball Scroll",
    consumable=consumable.FireballDamageConsumable(damage=12, radius=3),
)

dagger = Item(
    char="/", color=(0, 191, 255), name="Dagger", equippable=equippable.Dagger()
)

sword = Item(char="/", color=(0, 156, 207), name="Sword", equippable=equippable.Sword())

great_sword = Item(
    char="/",
    color=(0, 112, 149),
    name="Great Sword",
    equippable=equippable.GreatSword(),
)

leather_armor = Item(
    char="[",
    color=(139, 69, 19),
    name="Leather Armor",
    equippable=equippable.LeatherArmor(),
)

chain_mail = Item(
    char="[",
    color=(169, 169, 169),
    name="Chain Mail",
    equippable=equippable.ChainMail(),
)

plate_armor = Item(
    char="[",
    color=(232, 232, 232),
    name="Plate Armor",
    equippable=equippable.PlateArmor(),
)
