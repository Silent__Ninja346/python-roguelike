from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from caverns_of_wintermoor.engine import Engine
    from caverns_of_wintermoor.entity import Entity
    from caverns_of_wintermoor.game_map import GameMap


class BaseComponent:
    parent: Entity  # the entity owning this instance

    @property
    def game_map(self) -> GameMap:
        return self.parent.game_map

    @property
    def engine(self) -> Engine:
        return self.game_map.engine
